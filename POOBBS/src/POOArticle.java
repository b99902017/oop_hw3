
public class POOArticle extends POODirSys {
	private static final int AIDLEN = 3;
	public static int NEXT_AID = 0;
	private static String __table__ = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private String __art_id__;
	private String __title__;
	private String __author__;
	private String __content__;
	private int __eval_count__;
	public void retrieveAID() {
		int i, tmp = NEXT_AID;
		__art_id__ = "";
		for (i = 0; i < AIDLEN; i++)
		{
			__art_id__ += __table__.charAt(tmp % __table__.length());
			tmp /= __table__.length();
		}
		NEXT_AID++;
	}
	public POOArticle(String title, String author, String content) {
		super(title, "");
		__eval_count__ = 0;
		__title__ = title;
		__author__ = author;
		__content__ = content;
		__property__ = "article";
		retrieveAID();
	}
	public POOArticle() {
		this("", "", "");
	}
	public String getAID() {
		return __art_id__;
	}
	public void push(String msg) {
		__eval_count__++;
		super.add(new POOArtMsg(POOArtMsg.PUSH, msg));
	}
	public void boo(String msg) {
		__eval_count__--;
		super.add(new POOArtMsg(POOArtMsg.BOO, msg));
	}
	public void arrow(String msg) {
		super.add(new POOArtMsg(POOArtMsg.ARROW, msg));
	}
	public void show() {
		System.out.println("\033[2J");
		System.out.printf("\033[38;5;17;48;5;15mAuthor  \033[38;5;15;48;5;17m %-70s\033[m\n", __author__);
		System.out.printf("\033[38;5;17;48;5;15mTitle   \033[38;5;15;48;5;17m %-70s\033[m\n", __title__);
		System.out.println("\033[38;5;23m-------------------------------------------------------------------------------\033[m");
		System.out.println("");
		System.out.println("\t" + __content__);
		System.out.println("");
		System.out.println("\033[38;5;23m-----\033[m");
		System.out.println("");
		super.show();
		System.out.println("");
	}
	public void list(int pos) {
		System.out.printf("\033[38;5;15m%5d\033[m", pos);
		if (__eval_count__ > 0 && __eval_count__ < 10) System.out.printf("\033[38;5;10m%6d\033[m", __eval_count__);
		else if (__eval_count__ >= 10 && __eval_count__ < 100) System.out.printf("\033[38;5;11m%6d\033[m", __eval_count__);
		else if (__eval_count__ >= 100) System.out.printf("\033[38;5;196m boom!\033[m");
		else if (__eval_count__ <= 10 && -100 < __eval_count__) System.out.printf("    X%d", __eval_count__ / 10);
		else if (__eval_count__ <= -100) System.out.printf("    XX");
		else System.out.printf("%6d", __eval_count__);
		System.out.printf("%5s\033[38;5;14m%-10s\033[m%5s%-30s\n", "", __author__, "", __title__);
	}
}
