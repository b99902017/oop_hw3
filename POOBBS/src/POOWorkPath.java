
public class POOWorkPath extends POODirSys {
	public POOWorkPath() {
		super();
	}
	public boolean current_is(String property) {
		return current().getProterty().equals(property) == true;
	}
	public boolean current_isnot(String property) {
		return current().getProterty().equals(property) == false;
	}
	public boolean item_is(int pos, String property) {
		return current().getItem(pos).getProterty().equals(property) == true;
	}
	public boolean item_isnot(int pos, String property) {
		return current().getItem(pos).getProterty().equals(property) == false;
	}
	public int getCurrentNum() {
		return current().getNum();
	}
	public String getCurrentName() {
		return current().getName();
	}
	public POODirSys getCurrentItem(int pos) {
		return current().getItem(pos);
	}
	public boolean del() {
		if (__num__ > 1)
		{
			__num__--;
			return true;
		}
		return false;
	}
	public String list() {
		String res = "";
		POODirSys tmp_dir;
		for (int i = 0; i < __num__; i++) {
			tmp_dir = (POODirSys) __item_list__[i];
			res += "/" + tmp_dir.__name__;
		}
		return res;
	}
	public void list(int pos) {}
	public POODirSys current() {
		return (POODirSys) __item_list__[__num__ - 1];
	}
}
