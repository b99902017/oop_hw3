import java.util.Scanner;
import java.io.PrintStream;
import java.io.InputStream;

public class POOMain {
	/* ============================================================================================ */
	private static String cmd;
	private static String cmd_line = "";
	/* ============================================================================================ */
	private static final InputStream __stdin__ = System.in;
	private static final PrintStream __stdout__ = System.out;
	private static final PrintStream __stderr__ = System.err;
	private static Scanner __std_scan__ = new Scanner(System.in);
	private static Scanner __str_scan__;
	/* ============================================================================================ */
	private static boolean quit_flag;
	private static boolean demo_flag;
	private static int demo_state;
	private static POOWorkPath path = new POOWorkPath();
	private static POODirectory root = new POODirectory("root");
	/* ============================================================================================ */
	private static String read_next() {
		String res;
		if (demo_flag == true)	res = __str_scan__.next();
		else					res = __std_scan__.next();
		return res;
	}
	/* ============================================================================================ */
	private static boolean read_hasNextLine() {
		boolean res;
		if (demo_flag == true)	res = __str_scan__.hasNextLine();
		else					res = __std_scan__.hasNextLine();
		return res;
	}
	/* ============================================================================================ */
	private static String read_nextLine() {
		String res;
		if (demo_flag == true)	res = __str_scan__.nextLine();
		else					res = __std_scan__.nextLine();
		return res;
	}
	/* ============================================================================================ */
	private static int read_nextInt() {
		int res;
		if (demo_flag == true)	res = __str_scan__.nextInt();
		else					res = __std_scan__.nextInt();
		return res;
	}
	/* ============================================================================================ */
	private static void newline() {
		__stdout__.println("");
	}
	/* ============================================================================================ */
	private static void newline(int times) {
		for (int i = 0; i < times; i++)
			__stdout__.println("");
	}
	/* ============================================================================================ */
	private static void author() {
		__stdout__.println("\tThe system is developed by b99902017 (Karinsu).");
		newline();
		__stdout__.println("\tIf you have any question, please contact \033[4;38;5;40mb99902017@csie.ntu.edu.tw\033[m");
	}
	/* ============================================================================================ */
	private static void version_info() {
		newline();
		__stdout__.println("\t\033[38;5;12mCurrent version is 1.0.0 (Beta)\033[m");
		newline();
	}
	/* ============================================================================================ */
	private static void system_info(String arg) {
		__stdout__.println(" \033[38;5;11m[POO System]\033[m " + arg);
	}
	/* ============================================================================================ */
	private static void demo_info(String arg, Scanner scanner) {
		__stdout__.println("\033[38;5;46m" + path.list() + " >\033[m \033[38;5;11m(Demo)\033[m " + arg);
		scanner.nextLine();
	}
	/* ============================================================================================ */
	private static void demo_prompt(String arg) {
		__stdout__.println("\033[38;5;46m" + path.list() + " >\033[m \033[38;5;14m" + arg + "\033[m");
	}
	/* ============================================================================================ */
	private static void version(String ver) {
		if (ver.equals("0.0.3")) {
			newline();
			__stdout__.print("\033[38;5;13m0.0.3\033[m");
			__stdout__.println("\t\033[38;5;10mversion in 21:43, Apr 08, 2013\033[m");
			newline();
			__stdout__.println("\tConstruct the basic directory system (under construction).");
			__stdout__.println("\tArticle has push, boo, and arrow evaluation feature (under construction).");
			__stdout__.println("\tBasic UI had been setted.");
		}
		else if (ver.equals("0.1.0")) {
			newline();
			__stdout__.print("\033[38;5;13m0.1.0\033[m");
			__stdout__.println("\t\033[38;5;10mversion in 04:20, Apr 09, 2013\033[m");
			newline();
			__stdout__.println("\tBe able to build a directory in the directory.");
			__stdout__.println("\tBe able to cd the directory.");
			__stdout__.println("\tBe able to ls (or dir) the directory.");
		}
		else if (ver.equals("0.2.0")) {
			newline();
			__stdout__.print("\033[38;5;13m0.2.0\033[m");
			__stdout__.println("\t\033[38;5;10mversion in 21:05, Apr 09, 2013\033[m");
			newline();
			__stdout__.println("\tAdjust a make-up about showing directory.");
			__stdout__.println("\tAdd split line.");
			__stdout__.println("\tShow the working directory.");
			__stdout__.println("\tCd command can change directory with ID.");
		}
		else if (ver.equals("0.5.0")) {
			newline();
			__stdout__.print("\033[38;5;13m0.5.0\033[m");
			__stdout__.println("\t\033[38;5;10mversion in 03:41, Apr 12, 2013\033[m");
			newline();
			__stdout__.println("\tAdd article feature: post, del.");
			__stdout__.println("\tRevise the manual and add some instruction abbr.");
		}
		else if (ver.equals("0.6.0")) {
			newline();
			__stdout__.print("\033[38;5;13m0.6.0\033[m");
			__stdout__.println("\t\033[38;5;10mversion in 05:28, Apr 12, 2013\033[m");
			newline();
			__stdout__.println("\tRevise the article read.");
			__stdout__.println("\tStart add demo feature");
			__stdout__.println("\tAdd the feature of article aid, push, boo, and arrow.");
		}
		else if (ver.equals("0.7.0")) {
			newline();
			__stdout__.print("\033[38;5;13m0.7.0\033[m");
			__stdout__.println("\t\033[38;5;10mversion in 14:51, Apr 13, 2013\033[m");
			newline();
			__stdout__.println("\tDraw colors on Linux.");
		}
		else if (ver.equals("0.8.0")) {
			newline();
			__stdout__.print("\033[38;5;13m0.8.0\033[m");
			__stdout__.println("\t\033[38;5;10mversion in 21:50, Apr 13, 2013\033[m");
			newline();
			__stdout__.println("\tDemo program accomplished.");
			__stdout__.println("\tAdd move function.");
		}
		else if (ver.equals("1.0.0")) {
			newline();
			__stdout__.print("\033[38;5;13m1.0.0\033[m");
			__stdout__.println("\t\033[38;5;10mversion in 23:20, Apr 13, 2013\033[m");
			newline();
			__stdout__.println("\tAdd Makefile.");
			__stdout__.println("\tAdd report.");
		}
	}
	/* ============================================================================================ */
	private static void welcome() {
		newline();
		__stdout__.println("\tWelcome to POO BBS system!!!");
		__stdout__.println("\tThe system is recommended to running on Linux of R217 workstation.");
		version_info();
		author();
		newline();
		__stdout__.println("\tInput \033[38;5;196m\"demo\"\033[m to get an easy demonstrate.");
		newline(9);
	}
	/* ============================================================================================ */
	private static void history() {
		newline();
		__stdout__.println("\t\033[4;38;5;11mhistory:\033[m");
		version("0.0.3");
		version("0.1.0");
		version("0.2.0");
		version("0.5.0");
		version("0.6.0");
		version("0.7.0");
		version("0.8.0");
		version("1.0.0");
		newline();
	}
	/* ============================================================================================ */
	private static void manual_sys() {
		newline();
		__stdout__.println("\t\033[38;5;202mSystem instruction:\033[m");
		newline();
		__stdout__.println("\033[38;5;13m[ver|version]\033[m\t\t- show the current version of POO BBS system");
		__stdout__.println("\033[38;5;13m[his|history]\033[m\t\t- show the history about every version of POO BBS system");
		__stdout__.println("\033[38;5;13m[h|help]\033[m\t\t- the list of instuctions");
		__stdout__.println("\033[38;5;13m[man|manual]\033[m\t\t- the list of instuctions");
		__stdout__.println("\033[38;5;13mdemo\033[m\t\t\t- demo the program with easy tutorial");
		__stdout__.println("\033[38;5;13m[q|quit]\033[m\t\t- exit the POO BBS system");
		__stdout__.println("\033[38;5;13mexit\033[m\t\t\t- exit the POO BBS system");
	}
	/* ============================================================================================ */
	private static void manual_dir() {
		newline();
		__stdout__.println("\t\033[38;5;202mDirectory instruction:\033[m");
		newline();
		__stdout__.println("\033[38;5;13m[new|add] \033[38;5;14m-l\033[m\t\t- build a new split line");
		__stdout__.println("\033[38;5;13m[new|add] \033[38;5;14m-d <name>\033[m\t- build a new directory with a name");
		__stdout__.println("\033[38;5;13m[new|add] \033[38;5;14m-b <name>\033[m\t- build a new board with a name");
		__stdout__.println("\033[38;5;13m[d|del] \033[38;5;14m-n <num>\033[m\t- delete a board in the directory.");
		__stdout__.println("\033[38;5;13mcd \033[38;5;14m[<dir>|<b>|.|..]\033[m\t- go to the directory or board");
		__stdout__.println("\033[38;5;13mcd \033[38;5;14m-n <num>\033[m\t\t- go to the directory or board with a number argument");
		__stdout__.println("\033[38;5;13m[ls|dir]\033[m\t\t- list all directory and board in current path");
		__stdout__.println("\033[38;5;13m[m|move] \033[38;5;14m<src> <dest>\033[m\t- move the directory/board/split line from \033[38;5;12m<src>\033[m to \033[38;5;12m<dest>\033[m");
	}
	/* ============================================================================================ */
	private static void manual_bod() {
		newline();
		__stdout__.println("\t\033[38;5;202mBoard instruction:\033[m");
		newline();
		__stdout__.println("\033[38;5;13m[po|post]\033[m\t\t- post a new article in the board.");
		__stdout__.println("\033[38;5;13m[r|read] \033[38;5;14m-n <num>\033[m\t- read the \033[38;5;12m<num>\033[m'th article in the board.");
		__stdout__.println("\033[38;5;13m[d|del] \033[38;5;14m-n <num>\033[m\t- delete the \033[38;5;12m<num>\033[m'th article in the board.");
		__stdout__.println("\033[38;5;13m[ls|dir]\033[m\t\t- show all articles in the board.");
		__stdout__.println("\033[38;5;13m[m|move] \033[38;5;14m<src> <dest>\033[m\t- move the article line from \033[38;5;12m<src>\033[m to \033[38;5;12m<dest>\033[m");
	}
	/* ============================================================================================ */
	private static void manual_art() {
		newline();
		__stdout__.println("\t\033[38;5;202mArticle instruction:\033[m");
		newline();
		__stdout__.println("\033[38;5;13m[p|push] \033[38;5;14m-n <num>\033[m\t- push the \033[38;5;12m<num>\033[m'th article.");
		__stdout__.println("\033[38;5;13m[b|boo] \033[38;5;14m-n <num>\033[m\t- boo the \033[38;5;12m<num>\033[m'th article.");
		__stdout__.println("\033[38;5;13m[a|arrow] \033[38;5;14m-n <num>\033[m\t- arrow the \033[38;5;12m<num>\033[m'th article.");
		__stdout__.println("\033[38;5;13maid \033[38;5;14m-n <num>\033[m\t\t- show the Article ID of the \033[38;5;12m<num>\033[m'th article");
	}
	/* ============================================================================================ */
	private static void manual() {
		newline();
		__stdout__.println("\t\033[4;38;5;11mGolden manual/guidline:\033[m");
		manual_sys();
		manual_dir();
		manual_bod();
		manual_art();
		newline();
	}
	/* ============================================================================================ */
	private static String promptp() {
		__stdout__.print("\033[38;5;46m" + path.list() + " >\033[m ");
		return read_next();
	}
	private static String promptp(Scanner scanner) {
		__stdout__.print("\033[38;5;46m" + path.list() + " >\033[m ");
		return scanner.next();
	}
	/* ============================================================================================ */
	private static String prompt() {
		__stdout__.print(" \033[38;5;46m>\033[m ");
		return read_nextLine();
	}
	private static String prompt(Scanner scanner) {
		__stdout__.print(" \033[38;5;46m>\033[m ");
		return scanner.nextLine();
	}
	/* ============================================================================================ */
	private static String promptln(String arg) {
		__stdout__.print(arg);
		return read_nextLine();
	}
	private static String promptln(String arg, Scanner scanner) {
		__stdout__.print(arg);
		return scanner.nextLine();
	}
	/* ============================================================================================ */
	private static String promptSub(String arg) {
		String res = "", tmp = "";
		__stdout__.print(arg);
		do {
			res += tmp;
			tmp = prompt();
		} while (tmp.equals("\\end") == false);
		return res;
	}
	private static String promptSub(String arg, Scanner scanner) {
		String res = "", tmp = "";
		__stdout__.print(arg);
		do {
			res += tmp;
			tmp = prompt(scanner);
		} while (tmp.equals("\\end") == false);
		return res;
	}
	/* ============================================================================================ */
	private static void goodbye() {
		system_info("\033[38;5;208mGoodbye!!!\033[m");
	}
	/* ============================================================================================ */
	private static void sleep(long ms_sec) {
		try {
			Thread.sleep(ms_sec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}
	/* ============================================================================================ */
	public static void main(String[] args) {
		/*
		 * clean the monitor
		 */
		__stdout__.println("\033[2J");
		welcome();
		path.add(root);
		demo_flag = false;
		do
		{
			quit_flag = false;
			if (demo_flag == true) {
				/*
				 * demo_flag is open.
				 */
				switch (demo_state) {
				case 1:
					/*
					 * man
					 */
					__std_scan__.nextLine();
					demo_info("Welcome to demo mode. \033[38;5;15m<please ENTER>\033[m", __std_scan__);
					demo_info("Here we show the basic operartion about the POO BBS system. \033[38;5;15m<please ENTER>\033[m", __std_scan__);
					demo_info("First, if you want to get more instructions, you can enter \033[38;5;13m\"help\"\033[m or \033[38;5;13m\"manual\"\033[m to get the complete usage of the instructions. \033[38;5;15m<please ENTER>\033[m", __std_scan__);
					demo_info("The abbreivation about two of them are \033[38;5;13m\"h\"\033[m and \033[38;5;13m\"man\"\033[m. \033[38;5;15m<please ENTER>\033[m", __std_scan__);
					demo_info("First we use them to get the manual. \033[38;5;15m<please ENTER>\033[m", __std_scan__);
					cmd_line = "man";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 2:
					/*
					 * ls the directory
					 */
					sleep(500);
					demo_info("We can found all instructions in the manual. \033[38;5;15m<don\'t forgot ENTER :)>\033[m", __std_scan__);
					demo_info("Next, we show how to show the directory.", __std_scan__);
					demo_info("At the first, we are in the directory \033[38;5;13m\"/root\"\033[m. (also you can found yourself from leftside of every line)", __std_scan__);
					demo_info("If we want to list all items under the directory, we can use command \033[38;5;13m\"ls\"\033[m or \033[38;5;13m\"dir\"\033[m.", __std_scan__);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 3:
					/*
					 * new the board: OOP
					 */
					sleep(500);
					demo_info("If you want to add the board in the directory, we use \033[38;5;13m\"new\"\033[m or \033[38;5;13m\"add\"\033[m instruction.", __std_scan__);
					demo_info("The usage of these two instruction are the same.", __std_scan__);
					demo_info("The argument is as follows: \033[38;5;14m-l\033[m adds a new split line, \033[38;5;14m-d\033[m adds a new directory, and \033[38;5;14m-b\033[m adds a new board.", __std_scan__);
					demo_info("The arguments \033[38;5;14m-d\033[m and \033[38;5;14m-b\033[m should follow a \033[38;5;12mname\033[m as the name of directory/board.", __std_scan__);
					demo_info("Now we try to create a new board called \033[38;5;13m\"OOP\"\033[m", __std_scan__);
					cmd_line = "new -b OOP";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 4:
					sleep(500);
					demo_info("Then we can found \033[38;5;172mboard OOP\033[m is in the /root.", __std_scan__);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 5:
					sleep(500);
					demo_info("Also we can add more directory or split line.", __std_scan__);
					cmd_line = "add -l";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 6:
					sleep(500);
					cmd_line = "add -d OperatingSystem";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 7:
					sleep(500);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 8:
					sleep(500);
					demo_info("Now we have several directories and boards.", __std_scan__);
					demo_info("We can move thse items with instruction \033[38;5;13m\"move\"\033[m (abbr. \033[38;5;13m\"m\"\033[m).", __std_scan__);
					demo_info("The \033[38;5;13m\"move\"\033[m follows two arguments: \033[38;5;12m<src>\033[m and \033[38;5;12m<dest>\033[m.", __std_scan__);
					demo_info("For example, we move split line to the position \033[38;5;14m2\033[m.", __std_scan__);
					cmd_line = "move 1 2";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 9:
					sleep(500);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 10:
					sleep(500);
					demo_info("Also, we can move \033[38;5;172mboard OOP\033[m to the bottom with the number out of the range.", __std_scan__);
					cmd_line = "move 0 5";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 11:
					sleep(500);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 12:
					sleep(500);
					demo_info("Next, we try to enter the board (or directory), and then we use \033[38;5;13m\"cd\"\033[m.", __std_scan__);
					demo_info("\033[38;5;13m\"cd\"\033[m command can enter a \033[38;5;12mname\033[m to go into the \033[38;5;196mfirst\033[m match name of the board.", __std_scan__);
					demo_info("If you input a wrong name, the system will detect and found no board to match the name.", __std_scan__);
					cmd_line = "cd QQP";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 13:
					sleep(500);
					demo_info("Another usage of cd is that cd can follow the number of the directory/board.", __std_scan__);
					demo_info("Use argument \033[38;5;14m-n\033[m followed by a number to go to the \033[38;5;12mnum\033[m'th directory/board.", __std_scan__);
					cmd_line = "cd -n 2";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 14:
					sleep(500);
					demo_info("Now we enter the \033[38;5;202mboard OOP\033[m.", __std_scan__);
					demo_info("Board can use \033[38;5;13m\"ls\"\033[m or \033[38;5;13m\"dir\"\033[m to list all article, too.", __std_scan__);
					demo_info("But the difference between board and directory is the add new article.", __std_scan__);
					demo_info("An only way to add new article is \033[38;5;13m\"post\"\033[m command (abbr. \033[38;5;13m\"po\"\033[m).", __std_scan__);
					cmd_line = "po";
					demo_prompt(cmd_line);
					sleep(1000);
					cmd_line = "po\noop_hw3\nHT-L\noop hw3 is out\n\\end\n";
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 15:
					sleep(500);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 16:
					sleep(500);
					demo_info("We skip the creation of an article. The new artitle should contains \033[38;5;12mtitle\033[m, \033[38;5;12mauthor\033[m, and the \033[38;5;12mcontent\033[m.", __std_scan__);
					demo_info("The content has multiple lines and terminates with the symbol \033[38;5;196m\"\\end\"\033[m, the result won\'t contatin this symbol.", __std_scan__);
					demo_info("Then we can read the article with \033[38;5;13m\"read\"\033[m command (abbr. \033[38;5;13m\"r\"\033[m).", __std_scan__);
					demo_info("\033[38;5;13m\"read\"\033[m is also follows \033[38;5;14m-n\033[m and an article number.", __std_scan__);
					demo_info("Let\'s read what the article written about!!", __std_scan__);
					cmd_line = "read -n 0";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 17:
					sleep(500);
					demo_info("If you want to reply the article. Following 3 instructions can be used:", __std_scan__);
					demo_info("\033[38;5;13m\"push\"\033[m, \033[38;5;13m\"boo\"\033[m, and \033[38;5;13m\"arrow\"\033[m (abbr. \033[38;5;13m\"p\"\033[m, \033[38;5;13m\"b\"\033[m, \033[38;5;13m\"a\"\033[m, respectively).", __std_scan__);
					demo_info("all of them are follows \033[38;5;14m-n <num>\033[m, which indicates the \033[38;5;12m<num>\033[m\'th article.", __std_scan__);
					demo_info("Let\'s push an article.", __std_scan__);
					cmd_line = "push -n 0";
					demo_prompt(cmd_line);
					sleep(1000);
					cmd_line = "push -n 0\nNooooooooooo!!";
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 18:
					sleep(500);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 19:
					sleep(500);
					demo_info("If we push the article with message, the evaluation count will increase.", __std_scan__);
					demo_info("If the evaluation value is positive but less than 10, it will be \033[38;5;10mgreen\033[m.", __std_scan__);
					demo_info("If it\'s over 10 but not yet 100, the color will be \033[38;5;11myellow\033[m.", __std_scan__);
					demo_info("If it\'s over 100, there will show a string \033[38;5;196m\"boom\"\033[m with red.", __std_scan__);
					demo_info("Otherwise, if the evaluation is under -10 but not reach -100, it will be in the form \033[38;5;13m\"X<num>\"\033[m.", __std_scan__);
					demo_info("If it\'s under -100, it will show \033[38;5;13m\"XX\"\033[m for you", __std_scan__);
					demo_info("Other instructions are boo and arrow. The former decrease the evaluation, the latter doesn\'t influence it.", __std_scan__);
					demo_info("Next, we show that how to show the AID of the article.", __std_scan__);
					demo_info("AID is the absolute ID for the article, it isn\'t influenced by the deletion on the board.", __std_scan__);
					demo_info("The way to show the AID is \"\033[38;5;13maid \033[38;5;14m-n <num>\033[m\".", __std_scan__);
					cmd_line = "aid -n 0";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 20:
					sleep(500);
					demo_info("Although the AID system is unuse temporarily, but it will be useful if we continue developing the system :).", __std_scan__);
					demo_info("Next operation is to deiete the article.", __std_scan__);
					demo_info("We can delete the article by \033[38;5;13m\"del\"\033[m command.", __std_scan__);
					demo_info("\033[38;5;13m\"del\"\033[m command follows \033[38;5;14m-n <num>\033[m to delete the \033[38;5;12mnum\033[m\'th article arguments.", __std_scan__);
					cmd_line = "del -n 0";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 21:
					sleep(500);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 22:
					sleep(500);
					demo_info("If we want to back to the parent directory, we use \033[38;5;13m\"cd\"\033[m again.", __std_scan__);
					demo_info("the argument \033[38;5;13m\"..\"\033[m represents the parent directory, and \033[38;5;13m\".\"\033[m represents current directory or board.", __std_scan__);
					cmd_line = "cd ..";
					demo_prompt(cmd_line);
					sleep(1000);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 23:
					sleep(500);
					demo_info("Also, we can delete board and directory (and split line, too) with the same operation.", __std_scan__);
					cmd_line = "del -n 0";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 24:
					sleep(500);
					cmd_line = "del -n 0";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 25:
					sleep(500);
					cmd_line = "del -n 0";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 26:
					sleep(500);
					cmd_line = "ls";
					demo_prompt(cmd_line);
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				case 27:
					sleep(500);
					demo_info("The tutorial end in this part.", __std_scan__);
					demo_info("If you want to leave this BBS system, please use \033[38;5;13m\"quit\"\033[m or \033[38;5;13m\"exit\"\033[m.", __std_scan__);
					demo_info("The version info mation is in \033[38;5;13m\"version\"\033[m.", __std_scan__);
					demo_info("And \033[38;5;13m\"history\"\033[m will show the change of each version.", __std_scan__);
					demo_info("Thanks for reading this demo.", __std_scan__);
					demo_flag = false;
					cmd_line = "";
					__str_scan__ = new Scanner(cmd_line);
					sleep(500);
					break;
				default:
					cmd = "";
					demo_info("demaciaaaaaaaaaaaaaaaaaaaaaa!!!!!", __std_scan__);
					demo_info("", __std_scan__);
					demo_flag = false;
					break;
				}
				demo_state++;
			}
			/* ============================================================================================ */
			cmd = promptp();
			// __stderr__.println("cmd = " + cmd);
			/* ============================================================================================ */
			/*
			 * 
			 * system instruction
			 * 
			 */
			/* ============================================================================================ */
			if (cmd.equals("ver") || cmd.equals("version")) version_info();
			else if (cmd.equals("his") || cmd.equals("history")) history();
			else if (cmd.equals("h") || cmd.equals("help")) manual();
			else if (cmd.equals("man") || cmd.equals("manual")) manual();
			else if (cmd.equals("q") || cmd.equals("quit")) quit_flag = true;
			else if (cmd.equals("exit")) quit_flag = true;
			/* ============================================================================================ */
			/*
			 * 
			 * directory instruction
			 * 
			 */
			/* ============================================================================================ */
			else if (cmd.equals("new") || cmd.equals("add")) {
				if (path.current_isnot(POODirSys.PRO_DIR)) {
					system_info("You can only add new board or directory in the directories.");
					system_info("If you want to add an article you can use command \"post\"");
					cmd = read_nextLine();
				}
				else {
					cmd = read_next(); // -d | -b | -l
					newline();
					if (cmd.equals("-d")) {
						cmd = read_next();
						if (path.current().add(new POODirectory(cmd)))
							system_info("Add a new directory: " + cmd);
						else system_info("The number of directory and board is full.");
					}
					else if (cmd.equals("-b")) {
						cmd = read_next();
						if (path.current().add(new POOBoard(cmd)))
							system_info("Add a new board: " + cmd);
						else system_info("The number of directory and board is full.");
					}
					else if (cmd.equals("-l")) {
						if (path.current().add(new POOSplitLine()))
							system_info("Add a new splitting line.");
						else system_info("The number of directory and board is full.");
					}
					else {
						system_info("Usage: [new|add] -d <name>");
						system_info("Usage: [new|add] -b <name>");
						system_info("Usage: [new|add] -l");
					}
				}
			}
			/* ============================================================================================ */
			else if (cmd.equals("cd")) {
				int i = 0, pos0;
				cmd = read_next();
				newline();
				/*
				 * current directory
				 */
				if (cmd.equals(".")) {
					system_info("Stay in the same directory/board: " + path.list());
				}
				/*
				 * parent directory
				 */
				else if (cmd.equals("..")) {
					if (path.del()) system_info("Move to " + path.getCurrentName() + ".");
					else system_info("You are in the root.");
				}
				/*
				 * number argument
				 */
				else if (cmd.equals("-n")) {
					if (path.current_is("article")) {
						system_info("If you want to read article please use \"read -n <num>\".");
						cmd = read_nextLine();
					}
					else {
						pos0 = read_nextInt();
						int pos = pos0;
						if (pos < 0) pos = 0;
						if (pos >= path.getCurrentNum())
							pos = path.getCurrentNum() - 1;
						if (pos < path.getCurrentNum() && pos >= 0) {
							if (path.item_isnot(pos, POODirSys.PRO_LIN)) {
								if (path.add(path.getCurrentItem(pos)))
									system_info("Move to " + path.getCurrentName() + ".");
								else system_info("The number of directory and board in path is full.");
							}
							else system_info("This is a split line.");
						}
						else {
							system_info("No such directory or board ID: " + pos0);
							system_info("Usage: [cd|dir] [<dir>|<b>|.|..]");
							system_info("Usage: [cd|dir] -n <num>");
						}
					}
				}
				/*
				 * others
				 */
				else {
					for (i = 0; i < path.getCurrentNum(); i++) {
						if (path.getCurrentItem(i).getName().equals(cmd)) {
							if (path.item_isnot(i, POODirSys.PRO_LIN)) {
								if (path.add(path.getCurrentItem(i)))
									system_info("Move to " + path.getCurrentName() + ".");
								else system_info("The number of directory and board in path is full.");
							}
							else system_info("This is a split line.");
							i = -1;
							break ;
						}
					}
					if (i != -1) {
						system_info("No such directory or board: " + cmd);
						system_info("\033[38;5;202mUsage\033[m: [cd|dir] [<dir>|<b>|.|..]");
						system_info("\033[38;5;202mUsage\033[m: [cd|dir] -n <num>");
					}
				}
			}
			/* ============================================================================================ */
			else if (cmd.equals("d") || cmd.equals("del")) {
				cmd = read_next();
				if (cmd.equals("-n")) {
					int i;
					i = read_nextInt();
					if (path.getCurrentNum() > 0 && i >= 0 && i < path.getCurrentNum()) {
						if (path.current_is(POODirSys.PRO_ART)) {
							String title = path.getCurrentItem(i).getName();
							path.current().del(i);
							system_info("Delete article: " + title);
						}
						else if (path.current_is(POODirSys.PRO_BRD)) {
							String name = path.getCurrentItem(i).getName();
							path.current().del(i);
							system_info("Delete board: " + name);
						}
						else if (path.current_is(POODirSys.PRO_DIR)) {
							String name = path.getCurrentItem(i).getName();
							path.current().del(i);
							system_info("Delete directory: " + name);
						}
						else {
							system_info("Unknown error. Delete error.");
						}
					}
					else system_info("Out of the range: " + i);
				}
				else {
					system_info("Unknown argument: " + cmd);
					system_info("Usage: del -n <num>");
				}
			}
			/* ============================================================================================ */
			else if (cmd.equals("m") || cmd.equals("move")) {
				int i, j;
				i = read_nextInt();
				j = read_nextInt();
				if (path.getCurrentNum() > 0) {
					if (path.current().move(i, j)) {
						system_info("Move success.");
					}
					else {
						system_info("Move fail. No article at position " + i);
					}
				}
				else {
					if (path.current_is(POODirSys.PRO_ART)) system_info("There is no article.");
					else system_info("There is no board or directory.");
				}
			}
			/* ============================================================================================ */
			else if (cmd.equals("ls") || cmd.equals("dir")) {
				path.current().show();
			}
			/* ============================================================================================ */
			/*
			 * 
			 * board instruction
			 * 
			 */
			/* ============================================================================================ */
			else if (cmd.equals("po") || cmd.equals("post")) {
				if (path.current_isnot(POODirSys.PRO_BRD)) {
					system_info("You can only add new article in the boards.");
					cmd = read_nextLine();
				}
				else {
					String title, author, content;
					read_nextLine();
					/*
					 * title
					 */
					__stdout__.printf("\033[mTitle:  \033[38;5;0;48;5;7m%50s\033[m\033[50D\033[7m", "");
					title = read_nextLine();
					__stdout__.printf("\033[m");
					/*
					 * author
					 */
					__stdout__.printf("\033[mAuthor: \033[38;5;0;48;5;7m%50s\033[m\033[50D\033[7m", "");
					author = read_nextLine();
					__stdout__.printf("\033[m");
					/*
					 * content
					 */
					content = promptSub("\033[mcontent: (type \"\\end\" to terminate the input)\n");
					path.current().add(new POOArticle(title, author, content));
				}
			}
			/* ============================================================================================ */
			else if (cmd.equals("r") || cmd.equals("read")) {
				if (path.current_isnot(POODirSys.PRO_BRD)) {
					system_info("You can only read article in the boards.");
					cmd = read_nextLine();
				}
				else {
					cmd = read_next();
					if (cmd.equals("-n")) {
						int i;
						i = read_nextInt();
						if (i >= 0 && i < path.getCurrentNum()) {
							path.getCurrentItem(i).show();
						}
						else system_info("Out of range: " + i);
					}
					else {
						system_info("Unknown argument: " + cmd);
						system_info("Usage: [r|read] -n <num>");
					}
				}
			}
			/* ============================================================================================ */
			/*
			 * 
			 * article instruction
			 * 
			 */
			/* ============================================================================================ */
			else if (cmd.equals("p") || cmd.equals("b") || cmd.equals("a") || cmd.equals("push") || cmd.equals("boo") || cmd.equals("arrow")) {
				int type;
				if (cmd.equals("p") || cmd.equals("push")) type = POOArtMsg.PUSH;
				else if (cmd.equals("b") || cmd.equals("boo")) type = POOArtMsg.BOO;
				else type = POOArtMsg.ARROW;
				if (path.current_isnot(POODirSys.PRO_BRD)) {
					if (type == POOArtMsg.PUSH) system_info("You can only push an article in the boards.");
					else if (type == POOArtMsg.BOO) system_info("You can only boo an article in the boards.");
					else system_info("You can only arrow an article in the boards.");
					cmd = read_nextLine();
				}
				else {
					cmd = read_next();
					if (cmd.equals("-n")) {
						int i;
						String msg;
						i = read_nextInt();
						if (i >= 0 && i < path.getCurrentNum()) {
							POOArticle article = (POOArticle) path.getCurrentItem(i);
							read_nextLine();
							__stdout__.printf("Reply:\t\033[38;5;0;48;5;7m%50s\033[50D\033[m", "");
							msg = read_nextLine();
							if (type == POOArtMsg.PUSH) article.push(msg);
							else if (type == POOArtMsg.BOO) article.boo(msg);
							else article.arrow(msg);
						}
						else system_info("Out of range: " + i);
					}
					else {
						system_info("Unknown argument: " + cmd);
						if (type == POOArtMsg.PUSH) system_info("Usage: [p|push] -n <num>");
						else if (type == POOArtMsg.BOO) system_info("Usage: [b|boo] -n <num>");
						else system_info("Usage: [a|arrow] -n <num>");
					}
				}
				__stdout__.printf("\033[m");
			}
			/* ============================================================================================ */
			else if (cmd.equals("aid")) {
				if (path.current_isnot(POODirSys.PRO_BRD)) {
					system_info("You can only lookup AID of the article in the boards.");
					cmd = read_nextLine();
				}
				else {
					cmd = read_next();
					if (cmd.equals("-n")) {
						int i;
						i = read_nextInt();
						if (i >= 0 && i < path.getCurrentNum()) {
							POOArticle article = (POOArticle) path.getCurrentItem(i);
							newline();
							__stdout__.printf("\t\033[38;5;12mThe AID of the %d'th article is %s.\033[m\n", i, article.getAID());
							newline();
						}
						else system_info("Out of range: " + i);
					}
					else {
						system_info("\033[38;5;202mUnknown argument\033[m: " + cmd);
						system_info("\033[38;5;202mUsage\033[m: aid -n <num>");
					}
				}
			}
			/* ============================================================================================ */
			/*
			 * 
			 * demo
			 * 
			 */
			/* ============================================================================================ */
			else if (cmd.equals("demo")) {
				demo_flag = true;
				demo_state = 1;
			}
			/* ============================================================================================ */
			/*
			 * 
			 * help
			 * 
			 */
			/* ============================================================================================ */
			else system_info("Use \"h/help/man/manual\" command to retrieve the instructions.");
			/* ============================================================================================ */
		} while (quit_flag == false);
		goodbye();
	}
}
