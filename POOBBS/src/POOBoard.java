
public class POOBoard extends POODirSys {
	public POOBoard(String name) {
		//create a board with the name
		super(name, "board");
	}
	public POOBoard() {
		this("");
	}
	public void show() {
		//show the article titles of the board
		System.out.println("\033[2J");
		System.out.printf(" \033[38;5;11m[Article List]\033[38;5;12m%60s\033[m \n", "Current board: " + __name__);
		System.out.println("\033[38;5;0m-------------------------------------------------------------------------------\033[m");
		System.out.printf("\033[38;5;0;48;5;15m%6s%6s%4s%-10s%5s%-48s\033[m\n", "AID", "EVAL", "", "Author", "", "Title");
		super.show();
		System.out.println("");
	}
}
