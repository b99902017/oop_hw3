
public class POODirectory extends POODirSys {
	public POODirectory(String name) {
		//create a directory with the name
		super(name, "directory");
	}
	public POODirectory() {
		this("");
	}
	public void show() {
		//show the board/directory titles and splitting lines of the directory
		System.out.println("\033[2J");
		System.out.printf(" \033[38;5;11m[Board List]\033[38;5;12m%63s\033[m \n", "Current directory: " + __name__);
		System.out.println("\033[38;5;0m-------------------------------------------------------------------------------\033[m");
		System.out.printf("\033[38;5;0;48;5;15m%9s%5s%-50s%5s%-10s\033[m\n", "Broad ID", "", "Board", "", "Class");
		super.show();
		System.out.println("");
	}
}
