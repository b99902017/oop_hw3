
public class POOArtMsg extends POOItem {
	/**
	 * push, boo, or arrow
	 */
	public static int PUSH = 1;
	public static int BOO = -1;
	public static int ARROW = 0;
	int __type__;
	String __msg__;
	public POOArtMsg(int type, String msg) {
		__type__ = type;
		__msg__ = msg;
	}
	public POOArtMsg() {
		this(0, "");
	}
	public void list(int pos) {
		if (__type__ == PUSH) System.out.print("\033[38;5;11mpush\033[m\t- ");
		else if (__type__ == BOO) System.out.print("\033[38;5;1mboo\033[m\t- ");
		else if (__type__ == ARROW) System.out.print("\033[38;5;1m��\033[m\t- ");
		System.out.println(__msg__);
	}
}
