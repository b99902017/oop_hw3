
public abstract class POODirSys extends POOItem {
	public static final String PRO_ART = "article";
	public static final String PRO_BRD = "board";
	public static final String PRO_DIR = "directory";
	public static final String PRO_LIN = "----------";
	protected int __num__;
	protected String __name__, __property__;
	protected POOItem[] __item_list__;
	public POODirSys(String name, String proper) {
		__num__ = 0;
		__name__ = new String(name);
		__property__ = new String(proper);
		__item_list__ = new POOItem[MAXNUM];
	}
	public POODirSys() {
		this("", "");
	}
	public void setNum(int num) {
		__num__ = num;
	}
	public int getNum() {
		return __num__;
	}
	public void setName(String name) {
		__name__ = name;
	}
	public String getName() {
		return __name__;
	}
	public String getProterty() {
		return __property__;
	}
	public void setItem(int pos, POODirSys item) {
		__item_list__[pos] = item;
	}
	public POODirSys getItem(int pos) {
		return (POODirSys)__item_list__[pos];
	}
	public boolean add(POOItem item) {
		if (__num__ < MAXNUM) {
			__item_list__[__num__] = item;
			__num__++;
			return true;
		}
		return false;
	}
	public boolean del(int pos) {
		if (pos < 0 || pos >= __num__)
			return false;
		if (__num__ > 0)
		{
			for (int i = pos; i < __num__ - 1; i++)
				__item_list__[i] = __item_list__[i + 1];
			__num__--;
		}
		return true;
	}
	public boolean move(int src, int dest) {
		if (src >= __num__)
			return false;
		if (src < 0) src = 0;
		if (dest >= __num__) dest = __num__ - 1;
		if (src == dest)
			return true;
		POOItem tmp_item = __item_list__[src];
		if (src < dest) {
			for (int i = src; i < dest; i++)
				__item_list__[i] = __item_list__[i + 1];
		}
		else if (src > dest) {
			for (int i = src; i > dest; i--)
				__item_list__[i] = __item_list__[i - 1];
		}
		__item_list__[dest] = tmp_item;
		return true;
	}
	public void show() {
		for (int i = 0; i < __num__; i++)
			__item_list__[i].list(i);
	}
	public void list(int pos) {
		System.out.printf("\033[38;5;15m%8d\033[m%5s\033[38;5;14m%-50s\033[m%5s\033[38;5;13m%-10s\033[m\n", pos, "", __name__, "", __property__);
	}
	public int length() {
		return __num__;
	}
}